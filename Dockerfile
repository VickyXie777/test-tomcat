FROM tomcat:8.5.43
WORKDIR /usr/local/tomcat/webapps/
RUN rm -rf *
ADD . .
EXPOSE 8080
CMD ["/usr/local/tomcat/bin/catalina.sh", "run"]